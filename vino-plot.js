function loadAndPlot(url, callback) {
    $('#loading').show();
    $.ajax({ url: url,
        type: "GET",
        processData: false,
        contentType: false,
        dataType:"json",
        success: callback,
        complete: function(){
          $('#loading').hide();
        }
    })
    return false;
}
function bargridRectangle(x, y, xstep, ystep, colorline, fillcolor){
    var rectangle = {'type': 'rect', 'x0': x[0] - xstep, 'y0': y[0] - ystep, 'x1': x[1] + xstep, 'y1': y[1] + ystep, 'line': {'color': colorline, 'width': 1}, 'fillcolor': fillcolor};
    return rectangle;
}

function bargridPlot(data, container, colormarker, colorline, fillcolor) {
    var items = {x:[], y:[], mode:'markers', type: 'scatter', marker: { size : 2, color: colormarker }, hoverinfo:'none' };
    var trace = [];
    var tracebis = [];
    var x = [];
    var y = [];
    var map = [];
    var mapinit = [x, y];
    var mapitems = [];
    var mapinititems = [items.x, items.y];
    var xstep, ystep;
    var xbounds = [];
    var ybounds = [];
    $.each(data, function(index, bar) {
        if (index == 0){ // first row gives metadata: bounds, …
            xstep = bar[4] / 2;
            ystep = bar[5] / 2;
            xbounds.push(bar[0]);
            xbounds.push(bar[2]);
            ybounds.push(bar[1]);
            ybounds.push(bar[3]);
            map.push(mapinit[bar[6]]);
            map.push(mapinit[bar[7]]);
            mapitems.push(mapinititems[bar[6]]);
            mapitems.push(mapinititems[bar[7]]);
            x.push(0);
            x.push(1);
            y.push(0);
            y.push(1);
        } else {
            map[0][0] = bar[0];
            map[0][1] = bar[0];
            map[1][0] = bar[1];
            map[1][1] = bar[2];
            mapitems[0].push(bar[0]);
            mapitems[0].push(bar[0]);
            mapitems[1].push(bar[1]);
            mapitems[1].push(bar[2]);
            tracebis.push(bargridRectangle(x, y, xstep, ystep, colorline, fillcolor));
        }
    });
    var layout = {showlegend: false, margin:{t:10}, width: 600, height: 500,
      shapes : tracebis, xaxis:{title : 'X',range : xbounds},
      yaxis:{title : 'Y',range : ybounds}};
    trace = [items];
    Plotly.newPlot(container, trace, layout);
}
function bargridPlot3D(data, x, container, colormarker, colorline, fillcolor){
    var items = {x:[], y:[], z:[], mode:'markers', type: 'scatter3d', marker: { size : 1, color: [] }, hoverinfo:'none' };
    var trace = [];
    var tracebis = [];
    var x = [];
    var y = [];
    var z = [];
    var map = [];
    var mapinit = [x, y, z];
    var mapitems = [items.x, items.y, items.z];
    var mapinititems = [items.x, items.y, items.z];
    var xstep, ystep, zstep;
    var xbounds = [];
    var ybounds = [];
    var zbounds = [];
    $.each(data, function(index, bar) {
        if (bar.length > 4){
            xstep = bar[6] / 2;
            ystep = bar[7] / 2;
            zstep = bar[8] / 2;
            xbounds.push(bar[0]);
            xbounds.push(bar[3]);
            ybounds.push(bar[1]);
            ybounds.push(bar[4]);
            zbounds.push(bar[2]);
            zbounds.push(bar[5]);
            map.push(mapinit[bar[9]]);
            map.push(mapinit[bar[10]]);
            map.push(mapinit[bar[11]]);
            mapitems[0] = mapinititems[bar[9]];
            mapitems[1] = mapinititems[bar[10]];
            mapitems[2] = mapinititems[bar[11]];
            x.push(0);
            x.push(1);
            y.push(0);
            y.push(1);
            z.push(0);
            z.push(1);
        } else {
            map[0][0] = bar[0];
            map[0][1] = bar[0];
            map[1][0] = bar[1];
            map[1][1] = bar[1];
            map[2][0] = bar[2];
            map[2][1] = bar[3];
            mapitems[0].push(bar[0]);
            mapitems[0].push(bar[0]);
            mapitems[1].push(bar[1]);
            mapitems[1].push(bar[1]);
            mapitems[2].push(bar[2]);
            mapitems[2].push(bar[3]);
            items.marker.color.push(items.z[items.z.length - 2]);
            items.marker.color.push(items.z[items.z.length - 1]);
        }
    });
    var titles=[];
    //{% for ab in staab %}
    //    titles.push("{{ab}}");
    //{% endfor %}
    var layout = {showlegend: false, margin:{t:10}, width: 600, height: 500, scene : {xaxis:{title:titles[0],range : xbounds}, yaxis:{title:titles[1],range : ybounds}, zaxis:{title:titles[2],range : [0, 1]}}};
    trace = [items];
    Plotly.newPlot(container, trace, layout);
    var aes = [[1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 5]];
    var bes = [1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4];
    var ces = [1, 2, 3, 4, 2, 3, 4, 3, 3, 4, 3, 2, 4, 3, 2, 1];
    a = []; b = []; c = [];
    for (i = 0; i < 20; i++) {
        var a_ = Math.random();
        a.push(a_);
        var b_ = Math.random();
        b.push(b_);
        var c_ = Math.random();
        c.push(c_);
    }
    var dataessai = [{
        opacity:0.8,
        color:'rgb(200,100,300)',
        type: 'surface',
        z: aes,
    }];
}
